//---------------------------------------------------------------------------

#ifndef Point_Intersection_MainH
#define Point_Intersection_MainH
//---------------------------------------------------------------------------
#include <vcl.h>
#include <time.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <dos.h>
#include <dir.h>
#include <io.h>
#include "cspin.h"
#include <Buttons.hpp>
#include <Classes.hpp>
#include <Controls.hpp>
#include <ExtCtrls.hpp>
#include <StdCtrls.hpp>
//#include <algorithm.h>
//#include <Math.hpp>


#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "CSPIN.h"
#include <ExtCtrls.hpp>
#include <Buttons.hpp>
//---------------------------------------------------------------------------
typedef struct
{
	int x1, y1;     // start point of line
	int x2, y2;     // end point of line
}LineType;

typedef struct
{
	int x1,y1;      // center point of circle
	int r;          // radius of circle
}CircleType;

class TForm_Points_Intersection : public TForm
{
__published:	// IDE-managed Components
        TButton *Button_Start;
        TBitBtn *BitBtn_Close;
        TMemo *Memo_Log;
	TGroupBox *GroupBox1_Circle;
	TLabel *Label2;
	TEdit *Edit_Cx1;
	TEdit *Edit_Cy1;
	TLabel *Label3;
	TEdit *Edit_r;
	TGroupBox *GroupBox2;
	TLabel *Label4;
	TLabel *Label5;
	TEdit *Edit_Lx1;
	TEdit *Edit_Ly1;
	TEdit *Edit_Lx2;
	TEdit *Edit_Ly2;
        void __fastcall BitBtn_CloseClick(TObject *Sender);
        void __fastcall Button_StartClick(TObject *Sender);
private:	// User declarations
public:		// User declarations

		LineType xLine;
		CircleType xCircle;

		int IntersectionX1, IntersectionY1, IntersectionX2, IntersectionY2;

		int __fastcall findIntersection(LineType aLine, CircleType aCircle);
        __fastcall TForm_Points_Intersection(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm_Points_Intersection *Form_Points_Intersection;
//---------------------------------------------------------------------------
#endif
