object Form_Points_Intersection: TForm_Points_Intersection
  Left = 462
  Top = 111
  Caption = 'Points of Intersections'
  ClientHeight = 561
  ClientWidth = 631
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Button_Start: TButton
    Left = 40
    Top = 352
    Width = 345
    Height = 31
    Hint = 'Detect number of intersections'
    Caption = 'Find the Number of Intersections!'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Lucida Fax'
    Font.Style = []
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    OnClick = Button_StartClick
  end
  object BitBtn_Close: TBitBtn
    Left = 527
    Top = 352
    Width = 96
    Height = 31
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Lucida Fax'
    Font.Style = []
    Kind = bkClose
    NumGlyphs = 2
    ParentFont = False
    ParentShowHint = False
    ShowHint = False
    TabOrder = 1
    OnClick = BitBtn_CloseClick
  end
  object Memo_Log: TMemo
    Left = 0
    Top = 408
    Width = 631
    Height = 153
    Align = alBottom
    ScrollBars = ssBoth
    TabOrder = 2
    WordWrap = False
  end
  object GroupBox1_Circle: TGroupBox
    Left = 40
    Top = 192
    Width = 345
    Height = 138
    Caption = 'Define a Circle'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    object Label2: TLabel
      Left = 11
      Top = 40
      Width = 108
      Height = 24
      Caption = 'Center Point:'
    end
    object Label3: TLabel
      Left = 16
      Top = 96
      Width = 63
      Height = 24
      Caption = 'Radius:'
    end
    object Edit_Cx1: TEdit
      Left = 135
      Top = 32
      Width = 66
      Height = 32
      Hint = 'X Value'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
    end
    object Edit_Cy1: TEdit
      Left = 232
      Top = 32
      Width = 65
      Height = 32
      Hint = 'Y Value'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
    object Edit_r: TEdit
      Left = 135
      Top = 83
      Width = 66
      Height = 32
      Hint = 'radius value'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
    end
  end
  object GroupBox2: TGroupBox
    Left = 40
    Top = 32
    Width = 345
    Height = 137
    Caption = 'Define a Line'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    object Label4: TLabel
      Left = 16
      Top = 39
      Width = 88
      Height = 24
      Caption = 'Start Point:'
    end
    object Label5: TLabel
      Left = 16
      Top = 90
      Width = 87
      Height = 24
      Caption = 'End Point:'
    end
    object Edit_Lx1: TEdit
      Left = 135
      Top = 31
      Width = 66
      Height = 32
      Hint = 'X Value'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
    end
    object Edit_Ly1: TEdit
      Left = 232
      Top = 31
      Width = 65
      Height = 32
      Hint = 'Y Value'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
    object Edit_Lx2: TEdit
      Left = 135
      Top = 82
      Width = 66
      Height = 32
      Hint = 'X Value'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
    end
    object Edit_Ly2: TEdit
      Left = 232
      Top = 83
      Width = 65
      Height = 32
      Hint = 'Y Value'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
    end
  end
end
