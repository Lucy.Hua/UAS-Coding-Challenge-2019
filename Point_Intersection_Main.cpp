//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Point_Intersection_Main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma link "cspin"
#pragma resource "*.dfm"
TForm_Points_Intersection *Form_Points_Intersection;
//---------------------------------------------------------------------------
__fastcall TForm_Points_Intersection::TForm_Points_Intersection(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------


void __fastcall TForm_Points_Intersection::BitBtn_CloseClick(TObject *Sender)
{
  Close();
}
//---------------------------------------------------------------------------



void __fastcall TForm_Points_Intersection::Button_StartClick(TObject *Sender)
{
 int Result;
 int x;
 int y;
 x = Edit_Lx1->Text.ToInt();
 //if(x) to validate x
 xLine.x1 = x;
 x = Edit_Ly1->Text.ToInt();
 xLine.y1 = x;
 x = Edit_Lx2->Text.ToInt();
 xLine.x2 = x;
 x = Edit_Ly2->Text.ToInt();
 xLine.y2 = x;

 // Algorithm requires the y-coordinate of the start point to be above
 // the y-coordinate of the end point. A sorting algorithm is used to achieve this.
 if(xLine.y1 < xLine.y2)
  {
	x = xLine.x1;
	y = xLine.y1;

	xLine.x1 = xLine.x2;
	xLine.y1 = xLine.y2;

	xLine.x2 = x;
	xLine.y2 = y;
  }
  Memo_Log->Lines->Add("Reading line x1="+IntToStr(xLine.x1));
  Memo_Log->Lines->Add("Reading line y1="+IntToStr(xLine.y1));
  Memo_Log->Lines->Add("Reading line x2="+IntToStr(xLine.x2));
  Memo_Log->Lines->Add("Reading line y2="+IntToStr(xLine.y2));

 x = Edit_Cx1->Text.ToInt();
 Memo_Log->Lines->Add("Reading circle x1="+IntToStr(x));
 xCircle.x1 = x;
 x = Edit_Cy1->Text.ToInt();
 Memo_Log->Lines->Add("Reading circle y1="+IntToStr(x));
 xCircle.y1 = x;
 x = Edit_r->Text.ToInt();
 Memo_Log->Lines->Add("Reading circle r="+IntToStr(x));
 xCircle.r = x;

 Result = findIntersection(xLine, xCircle);

}
//---------------------------------------------------------------------------

// A function that determines the number of interesctions and the coordinates for those points
int __fastcall TForm_Points_Intersection::findIntersection(LineType aLine, CircleType aCircle)
{
 int xResult=0;
 double Cxy1, Cxy2;   // Potential points of intersect when dealing with verticle line
 double m;            // Slope of Line
 double b;            // y-intercept of Line
 double d;            // Discriminant for when the equation of the line is set to be
				   // equal to the equation of the circle

 AnsiString xMsgStr;   //To store a message that will be displayed in message box

 if(aLine.x1 == aLine.x2)    // When determining slope, cannot divide by zero. Must use different algorithm than the block below.
   {
   if((aLine.x1 > (aCircle.x1 + aCircle.r)) ||          // Verticle line is to the right or left
	  (aLine.x1 < (aCircle.x1 - aCircle.r)))           	// of the circle respectively

	 {
	   MessageBox(Handle,"There is NO point of intersection", "Results: Verticle Line", MB_OK);
	   return(xResult);
	 }

	 // Determining the potential points of intersection: (aLine.x1, Cxy1) and (aLine.x2, Cxy2)
	 Cxy1 = aCircle.y1 + sqrt(pow(double(aCircle.r),2) -
			pow(double(aLine.x1 - aCircle.x1),2));

	 Cxy2 = aCircle.y1 - sqrt(pow(double(aCircle.r),2) -
			pow(double(aLine.x1 - aCircle.x1),2));


   if((aLine.x1 == (aCircle.x1 + aCircle.r)) ||         // Verticle line is tangent to the right or left
	  (aLine.x1 == (aCircle.x1 - aCircle.r)))           // side of the circle respectively

	 {
	   if((aLine.y1 != Cxy1 && aLine.y2 == Cxy2)||      // The end of the line segment just touches the circle
		  (aLine.y1 == Cxy1 && aLine.y2 != Cxy2)||      // The start of the line segment just touches the circle
		  (aLine.y1 > Cxy1  && aLine.y2 < Cxy2))        // A point on the line segmnet just touches the circle
		 {
		   IntersectionX1 = aLine.x1;
		   IntersectionY1 = Cxy1;
		   xMsgStr = "There is ONE point of intersection, at: ("
					   + IntToStr(IntersectionX1) + ", " + IntToStr(IntersectionY1) + ").";

		   MessageBox(Handle, xMsgStr.c_str(), "Results: Verticle Line", MB_OK);
		   xResult = 1;

		   return(xResult);
		  }
		  else    //Line segment is above or below the circle
		  {
		   MessageBox(Handle,"There is NO point of intersection", "Results: verticle line", MB_OK);
		   return(xResult);
		  }
	 }
   if((aLine.x1 < (aCircle.x1 + aCircle.r)) && (aLine.x1 > (aCircle.x1 - aCircle.r)))  // Horizontal position of vertical line
																					   // is within the bounds of the circle
	{
	   if((aLine.y1 != Cxy1 && aLine.y2 == Cxy2) ||      // The end point of the line segment is on the circle, but not intersecting
		  (aLine.y1 == Cxy1 && aLine.y2 != Cxy2))        // The start point of the line segment is on the circle, but not interseting
		   {
			 MessageBox(Handle,"There is NO point of intersection", "Results: Verticle Line", MB_OK);
			 return(xResult);
		   }
		   else if (aLine.y1 >= Cxy1 && aLine.y2 <= Cxy2)// The line segment goes through the circle
		   {
			 IntersectionX1 = aLine.x1;
			 IntersectionY1 = Cxy1;

			 IntersectionX2 = aLine.x2;
			 IntersectionY2 = Cxy2;

			 xMsgStr = "There are TWO points of intersection, at: ("
						+ IntToStr(IntersectionX1) + ", " + IntToStr(IntersectionY1) + ")" +
						" and ("
						+ IntToStr(IntersectionX2) + ", " + IntToStr(IntersectionY2) + ").";

			 MessageBox(Handle, xMsgStr.c_str(), "Results: Verticle Line", MB_OK);
			 xResult = 2;

			 return(xResult);
		   }
		   else
           {
			 MessageBox(Handle,"Invalid Coordinates: Point lies within circle", "Results: Verticle Line", MB_OK);
			 return(xResult);
		   }

	}

   }


 // Formulas for finding Slope, y-intercept, and discriminant respectively
 m = (aLine.y2 - aLine.y1)*1.0/(aLine.x2 - aLine.x1);
 Memo_Log->Lines->Add("Reading m="+FloatToStrF(m,ffFixed, 4, 2));

 b = (aLine.y2 - aLine.x2 * m);

 Memo_Log->Lines->Add("Reading b="+FloatToStr(b));

 d = pow(double(aCircle.r),2) * (1 + pow(double(m),2)) -
	 pow((double(aCircle.y1 - m * aCircle.x1 - b)),2);

 Memo_Log->Lines->Add("Reading d="+FloatToStr(d));

 // Using the discriminant to determine the number of solutions,
 // or in other words, the number of intersections between the line and circle
 if(d == 0)  // There is one solution
	{
	// Determining coordinates of intersection: (x1, y1)
	IntersectionX1 = (aCircle.x1 + aCircle.y1 * m - b * m)
					   / (1 + pow(double(m), 2));
	IntersectionY1 = (b + aCircle.x1 * m + aCircle.y1 * pow(double(m), 2))
					   / (1 + pow(double(m), 2));

	xMsgStr = "There is ONE point of intersection, at: ("
			   + IntToStr(IntersectionX1) + ", " + IntToStr(IntersectionY1) + ").";

	MessageBox(Handle, xMsgStr.c_str(), "Results:", MB_OK);
	xResult = 1;
	}
	else if(d > 0)  // There are two solutions
	{
	// Determining coordinates of the two intersections: (x1, y1) and (x2, y2)
	IntersectionX1 = (aCircle.x1 + aCircle.y1 * m - b * m + sqrt(double(d)))
					  / (1 + pow(double(m), 2));
	IntersectionY1 = (b + aCircle.x1 * m + aCircle.y1 * pow(double(m), 2) + m * sqrt(double(d)))
					  / (1 + pow(double(m), 2));

	IntersectionX2 = (aCircle.x1 + aCircle.y1 * m - b * m - sqrt(double(d)))
					  / (1 + pow(double(m), 2));
	IntersectionY2 = (b + aCircle.x1 * m - aCircle.y1 * pow(double(m), 2) + m * sqrt(double(d)))
					  / (1 + pow(double(m), 2));

	xMsgStr = "There are TWO points of intersection, at: ("
			   + IntToStr(IntersectionX1) + ", " + IntToStr(IntersectionY1) + ")" +
			   " and ("
			   + IntToStr(IntersectionX2) + ", " + IntToStr(IntersectionY2) + ").";

	MessageBox(Handle, xMsgStr.c_str(), "Results:", MB_OK);
	xResult = 2;
	}
	else  // d<0; there are no solutions
	{
	MessageBox(Handle,"There is NO point of intersection", "Results:", MB_OK);
	}

 return(xResult);
}
